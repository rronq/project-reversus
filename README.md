# Project Reversus

Project Reversus is a Capstone project created for the Emerging Analytics Center Cyber Gym. It contains programs that are meant to be cracked in an attempt to teach students about binary analysis and reverse engineering. All of the crackthis programs are developed in C with the binaries compiled with minGW. 

### Note: This may work with Visual Studio 2019 if settings are adjusted. Some functions like getch() will prevent the code from being compiled due to deprecation.

## Hack the Shapes
Hack the Shapes is the final part of Project Reversus in the form of a game. Players must hack the game in order to beat it. In it, there are three separate challenges that will require knowledge of value editing in memory as well as finding player coordinates. 

Recommended tools for solving reversus problems: Cheat Engine, HxD, Ghidra, IDA
